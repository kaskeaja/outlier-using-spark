import unittest

from development_dataset import *

class TestHelpers(unittest.TestCase):
  def test_pickRange(self):
    upper_limits = [0.1, 0.2, 0.3, 0.5, 0.7, 0.95, 1]

    self.assertEqual(Helpers.pickRange(0.05, upper_limits), 0)
    self.assertEqual(Helpers.pickRange(0.15, upper_limits), 1)
    self.assertEqual(Helpers.pickRange(0.5, upper_limits), 3)
    self.assertEqual(Helpers.pickRange(0.99, upper_limits), 6) 

  def test_createCumulativeDensityFunction(self):
    probabilities = [0.1, 0.2, 0.3, 0.4]

    cdf = Helpers.createCumulativeDensityFunction(probabilities)
    self. assertAlmostEqual(cdf[0], 0.1)
    self. assertAlmostEqual(cdf[1], 0.3)
    self. assertAlmostEqual(cdf[2], 0.6)
    self. assertAlmostEqual(cdf[3], 1)

class TestAttributeValue(unittest.TestCase):
  def test_basic_functionality(self):
    # Let's create AttributeValue
    possible_values = []
    possible_values.append({'value': 'A', 'probability': 0.1})
    possible_values.append({'value': 'B', 'probability': 0.2})
    possible_values.append({'value': 'C', 'probability': 0.3})
    possible_values.append({'value': 'D', 'probability': 0.4})

    value = AttributeValue("label", possible_values)

    # Let's evaluate the value X times
    number_of_test = 100000
    results = []
    for ii in range(0, number_of_test):
      results.append(value.evaluate())

    # Let's check the results
    values = {}
    for item in results:
      values[item] = values.get(item, 0) + 1

    required_decimals = 2
    self. assertAlmostEqual(float(values['label: A'])/float(number_of_test), 0.1, required_decimals)
    self. assertAlmostEqual(float(values['label: B'])/float(number_of_test), 0.2, required_decimals)
    self. assertAlmostEqual(float(values['label: C'])/float(number_of_test), 0.3, required_decimals)
    self. assertAlmostEqual(float(values['label: D'])/float(number_of_test), 0.4, required_decimals)

class TestAttributeValueChain(unittest.TestCase):
  def test_basic_functionality(self):
    possible_values_A = []
    possible_values_A.append({'value': 'A', 'probability': 0.1})
    possible_values_A.append({'value': 'B', 'probability': 0.8})
    possible_values_A.append({'value': 'C', 'probability': 0.1})
    A = AttributeValue('labelA', possible_values_A)

    possible_values_B = []
    possible_values_B.append({'value': 'C', 'probability': 0.3})
    possible_values_B.append({'value': 'D', 'probability': 0.6})
    possible_values_B.append({'value': 'E', 'probability': 0.1})
    B = AttributeValue('labelB', possible_values_B)

    chain = AttributeValueChain([A, B])

    number_of_test = 50000
    # Lets evaluate the chain X times
    results = []
    for ii in range(0, number_of_test):
      results.append(chain.evaluate())

    # Lets check the results
    values_A = {}
    values_B = {}
    for item in results:
      values_A[item[0]] = values_A.get(item[0], 0) + 1
      values_B[item[1]] = values_B.get(item[1], 0) + 1

    # The first value in the chain
    required_decimals = 2
    self. assertAlmostEqual(float(values_A['labelA: A'])/float(number_of_test), 0.1, required_decimals)
    self. assertAlmostEqual(float(values_A['labelA: B'])/float(number_of_test), 0.8, required_decimals)
    self. assertAlmostEqual(float(values_A['labelA: C'])/float(number_of_test), 0.1, required_decimals)

    # The second value in the chain
    self. assertAlmostEqual(float(values_B['labelB: C'])/float(number_of_test), 0.3, required_decimals)
    self. assertAlmostEqual(float(values_B['labelB: D'])/float(number_of_test), 0.6, required_decimals)
    self. assertAlmostEqual(float(values_B['labelB: E'])/float(number_of_test), 0.1, required_decimals)

class TestDataset(unittest.TestCase):
  def test_basic_functionality(self):

    # Let's use static chains to test the dataset generation

    A = AttributeValue("labelA", [{"value": "A1", 'probability': 1}])
    B = AttributeValue("labelB", [{"value": "B1", 'probability': 1}])
    chain1 = AttributeValueChain([A, B])

    A = AttributeValue("labelA", [{"value": "A2", 'probability': 1}])
    B = AttributeValue("labelB", [{"value": "B2", 'probability': 1}])
    chain2 = AttributeValueChain([A, B])

    A = AttributeValue("labelA", [{"value": "A3", 'probability': 1}])
    B = AttributeValue("labelB", [{"value": "B3", 'probability': 1}])
    chain3 = AttributeValueChain([A, B])

    number_of_test = 100000
    dataset_generator = Dataset(number_of_test)
    dataset_generator.addAttributeValueChain(chain1, 0.1)
    dataset_generator.addAttributeValueChain(chain2, 0.8)
    dataset_generator.addAttributeValueChain(chain3, 0.1)

    dataset = dataset_generator.evaluate()

    values = {}
    # Let's check the results
    for line in dataset:
      parts = line.split(',')
      values[parts[0]] = values.get(parts[0], 0) + 1

    required_decimals = 2
    self.assertAlmostEqual(values['labelA: A1']/float(number_of_test), 0.1, required_decimals)
    self.assertAlmostEqual(values['labelA: A2']/float(number_of_test), 0.8, required_decimals)
    self.assertAlmostEqual(values['labelA: A3']/float(number_of_test), 0.1, required_decimals)

if __name__ == '__main__':
    unittest.main()