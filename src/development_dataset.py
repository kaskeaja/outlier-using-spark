# -*- coding: utf-8 -*-

import random
import math
"""
Module to create a dataset for development purposes.
"""

class Dataset(object):
  """
  Dataset encapsulates artificial dataset creation and storing.  
  """
  def __init__(self, number_of_points):
    """
    Construct a new 'Dataset' object.

    :param  number_of_points: Number of points (rows) in the dataset.
    :returns: Nothing.
    """

    self._number_of_points = number_of_points
    self._chains = []
    self._cdf = []

  def addAttributeValueChain(self, attribute_value_chain, probability):
    """
    Adds attribute value chain to the dataset

    :param  attribute_value_chain: :class:'AttributeValueChain'.
    :param  frequency: relative frequency of the attribute value chain in the dataset.
    :returns: Nothing.
    """
    
    self._chains.append(attribute_value_chain)
    self._cdf.append(probability) # will be fixed before evaluation (turned to cdf)

  def evaluate(self):
    """
    Evaluates the dataset.

    :returns: List if strings.
    """

    self._cdf = Helpers.createCumulativeDensityFunction(self._cdf)

    # evaluating the dataset
    dataset = []
    for ii in range(0, self._number_of_points):
      # Let's pick chain in random
      chain_index = Helpers.pickRange(random.random(), self._cdf)
      chain = self._chains[chain_index].evaluate()
      dataset_line = ""
      for item in chain:
        dataset_line += item + ", "

      dataset_line = dataset_line[:-2] # removes extra ', ' at the end of the line

      dataset.append(dataset_line)

    return dataset

class AttributeValue(object):
  """
  Encapsulates picking a value for an attribute.
  """
  def __init__(self, label, possible_values):
    """
    Random variable that can take any value defined in 'possible_values' (with defined probability).

    :param  label: string label of the attribute value.
    :param  possible_values: array of posible values and probabilities. I.e. [..,{value: 'A', probability: 0.5},..].
    :returns: Nothing.
    """

    self._possible_values = possible_values
    self._label = label;

    probabilities = []
    for item in self._possible_values:
      probabilities.append(item['probability'])    
    self._cdf = Helpers.createCumulativeDensityFunction(probabilities)

  def evaluate(self):
    """
    Evaluates attribute random variable to string.

    :returns: Nothing.
    """

    # Let's pick value in random.
    value_index = Helpers.pickRange(random.random(), self._cdf)

    return self._label + ": " + str(self._possible_values[value_index]['value'])
    
class AttributeValueChain(object):
  """
  Encapsulates creating a value of attribute values. 
  """
  def __init__(self, values):  
    """
    Chain of attribute values.

    :param  values: List of :class::'AttributeValue's.
    :returns: Nothing.
    """

    self._values = values

  def evaluate(self):
    """
    Evaluates AttributeValueChain.
    :returns: List of :class::AttributeValues
    """

    retval = []
    for item in self._values:
      retval.append(item.evaluate())

    return retval
    
class Helpers(object):
  """
  Miscellaneous helper functions.
  """
  
  @staticmethod
  def createCumulativeDensityFunction(probabilities):
    """
    Turns probability density to cumulative probability density function. If sum of the probabilities is > 1 then
    probabilities are scaled.

    :param  probabilities: list of probabilities of the classes.
    :returns: cumulative density function of the classes as a list.
    """
    sum_of_probabilities = 0;
    for item in probabilities:
      sum_of_probabilities += item

    cdf = []
    cumulative = 0
    for ii in range(0, len(probabilities)):
      cumulative += probabilities[ii]/sum_of_probabilities 
      cdf.append(cumulative)

    return cdf

  @staticmethod
  def pickRange(value, poles):
    """
    Chain of attribute values.

    :param  value: A number in range [0, 1].
    :param  poles: List of borders between the ranges. 
    :returns: Index of range 'random_variable_realization' belongs to.
    """
    
    # Trivial case
    if len(poles) == 1:
      return 0
    
    # Lets check if answer is the first or the last range
    if value < poles[0]:
      return int(0)
    if (value >= poles[-2]):
      return int(len(poles) - 1)

    lower = 0
    upper = int(len(poles) - 2) # the last range is already checked

    while True:
      upper_or_lower = int(math.floor(0.5*(upper + lower)))
      if poles[upper_or_lower] < value:
        lower = upper_or_lower
      else:
        upper = upper_or_lower

      if lower + 1 == upper:
        # range is found
        break 
  
    return upper

if __name__ == '__main__':
  # Lets create attribute values.

  # Chain 1
  possible_values_Attribute_A_chain1 = []
  possible_values_Attribute_A_chain1.append({'value': 'c1A', 'probability': 0.1})
  possible_values_Attribute_A_chain1.append({'value': 'c1B', 'probability': 0.2})
  possible_values_Attribute_A_chain1.append({'value': 'c1C', 'probability': 0.3})
  possible_values_Attribute_A_chain1.append({'value': 'c1D', 'probability': 0.3})
  possible_values_Attribute_A_chain1.append({'value': 'c1E', 'probability': 0.1})
  A_chain1 = AttributeValue('labelA', possible_values_Attribute_A_chain1)

  possible_values_Attribute_B_chain1 = []
  possible_values_Attribute_B_chain1.append({'value': 'c1AA', 'probability': 0.1})
  possible_values_Attribute_B_chain1.append({'value': 'c1BB', 'probability': 0.2})
  possible_values_Attribute_B_chain1.append({'value': 'c1CC', 'probability': 0.3})
  possible_values_Attribute_B_chain1.append({'value': 'c1DD', 'probability': 0.3})
  possible_values_Attribute_B_chain1.append({'value': 'c1EE', 'probability': 0.1})
  B_chain1 = AttributeValue('labelB', possible_values_Attribute_B_chain1)

  possible_values_Attribute_C_chain1 = []
  possible_values_Attribute_C_chain1.append({'value': 'c1AAA', 'probability': 0.1})
  possible_values_Attribute_C_chain1.append({'value': 'c1BBB', 'probability': 0.2})
  possible_values_Attribute_C_chain1.append({'value': 'c1CCC', 'probability': 0.3})
  possible_values_Attribute_C_chain1.append({'value': 'c1DDD', 'probability': 0.3})
  possible_values_Attribute_C_chain1.append({'value': 'c1EEE', 'probability': 0.1})
  C_chain1 = AttributeValue('labelC', possible_values_Attribute_C_chain1)

  chain_1 = AttributeValueChain([A_chain1, B_chain1, C_chain1])

  # Chain 2
  possible_values_Attribute_A_chain2 = []
  possible_values_Attribute_A_chain2.append({'value': 'c2A', 'probability': 0.9})
  possible_values_Attribute_A_chain2.append({'value': 'c2B', 'probability': 0.1})
  A_chain2 = AttributeValue('labelA', possible_values_Attribute_A_chain2)

  possible_values_Attribute_B_chain2 = []
  possible_values_Attribute_B_chain2.append({'value': 'c2AA', 'probability': 0.1})
  possible_values_Attribute_B_chain2.append({'value': 'c2BB', 'probability': 0.2})
  possible_values_Attribute_B_chain2.append({'value': 'c2CC', 'probability': 0.3})
  possible_values_Attribute_B_chain2.append({'value': 'c2DD', 'probability': 0.3})
  possible_values_Attribute_B_chain2.append({'value': 'c2EE', 'probability': 0.1})
  B_chain2 = AttributeValue('labelB', possible_values_Attribute_B_chain2)

  possible_values_Attribute_C_chain2 = []
  possible_values_Attribute_C_chain2.append({'value': 'c2AAA', 'probability': 0.1})
  possible_values_Attribute_C_chain2.append({'value': 'c2BBB', 'probability': 0.2})
  possible_values_Attribute_C_chain2.append({'value': 'c2CCC', 'probability': 0.3})
  possible_values_Attribute_C_chain2.append({'value': 'c2DDD', 'probability': 0.3})
  possible_values_Attribute_C_chain2.append({'value': 'c2EEE', 'probability': 0.1})
  C_chain2 = AttributeValue('labelC', possible_values_Attribute_C_chain2)

  chain_2 = AttributeValueChain([A_chain2, B_chain2, C_chain2])

  # Chain 3
  possible_values_Attribute_A_chain3 = []
  possible_values_Attribute_A_chain3.append({'value': 'c3A', 'probability': 0.9})
  possible_values_Attribute_A_chain3.append({'value': 'c3B', 'probability': 0.1})
  A_chain3 = AttributeValue('labelA', possible_values_Attribute_A_chain3)

  possible_values_Attribute_B_chain3 = []
  possible_values_Attribute_B_chain3.append({'value': 'c3AA', 'probability': 0.4})
  possible_values_Attribute_B_chain3.append({'value': 'c3BB', 'probability': 0.2})
  possible_values_Attribute_B_chain3.append({'value': 'c3EE', 'probability': 0.2})
  B_chain3 = AttributeValue('labelB', possible_values_Attribute_B_chain3)

  possible_values_Attribute_C_chain3 = []
  possible_values_Attribute_C_chain3.append({'value': 'c3AAA', 'probability': 0.9})
  possible_values_Attribute_C_chain3.append({'value': 'c3EEE', 'probability': 0.1})
  C_chain3 = AttributeValue('labelC', possible_values_Attribute_C_chain3)

  chain_3 = AttributeValueChain([A_chain3, B_chain3, C_chain3])

  dataset_generator = Dataset(100000)
  dataset_generator.addAttributeValueChain(chain_1, 0.50)
  dataset_generator.addAttributeValueChain(chain_2, 0.49)
  dataset_generator.addAttributeValueChain(chain_3, 0.001)
  
  dataset = dataset_generator.evaluate()

  f = open('../data/development_dataset.txt', 'w')
  for item in dataset:
    f.write(item + '\n')
  f.close()