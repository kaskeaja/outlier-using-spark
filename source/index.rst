.. Outlier detection documentation master file, created by
   sphinx-quickstart on Thu Nov 19 00:19:32 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Outlier detection's documentation!
=============================================

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: development_dataset
  
  .. autoclass:: Dataset
    :members:

    .. automethod:: __init__

  .. autoclass:: AttributeValue
    :members:

    .. automethod:: __init__


  .. autoclass:: AttributeValueChain
    :members:

    .. automethod:: __init__

  .. autoclass:: Helpers
    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

